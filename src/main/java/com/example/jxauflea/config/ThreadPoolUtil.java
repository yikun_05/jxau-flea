package com.example.jxauflea.config;

import com.example.jxauflea.util.Context;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author chenzouquan
 */
@Configuration
@EnableAsync
public class ThreadPoolUtil implements AsyncConfigurer {
    @Override
    public Executor getAsyncExecutor() {
         return  new ThreadPoolExecutor(
                Context.CORE_POOL_SIZE,
                Context.MAXIMUM_POOL_SIZE,
                Context.KEEP_ALIE_TIME,
                Context.UNIT,
                Context.workQueue,
                 Context.handler);
    }
}

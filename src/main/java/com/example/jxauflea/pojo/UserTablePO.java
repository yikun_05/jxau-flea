package com.example.jxauflea.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserTablePO {

        private String uuid;
        private String userId;
        private Date modifiedTime;
        private String password;

}

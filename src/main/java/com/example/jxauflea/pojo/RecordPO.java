package com.example.jxauflea.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecordPO {

        private String uuid;
        private String goodsId;
        private String type;
        private Integer quantity;// 商品数量
}

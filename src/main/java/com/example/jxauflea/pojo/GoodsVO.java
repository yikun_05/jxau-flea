package com.example.jxauflea.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoodsVO {

    private String uuid;
    private String goodsName;
    private double price;
    private String userId;
    private String description;
    private Date modifiedTime;
    private String type;
    private Integer state;
    private Integer quantity;
    private Date createTime;
    private String userName;
    private String userImg;
    private String time;
    private List<String> imgUrls;
    private Boolean isCollected;
}

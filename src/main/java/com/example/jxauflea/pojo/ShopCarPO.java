package com.example.jxauflea.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShopCarPO {

        private String uuid;
        private String goodsId;
        private String userId;
        private Date createTime;
        private Integer quantity;// 商品数量
}

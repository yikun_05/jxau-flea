package com.example.jxauflea.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoodsListVO {

    private String uuid;
    private String goodsName;
    private String goodsImgUrl;

}

package com.example.jxauflea.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author chenzouquan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address {
    private String name;
    private String tel;
    private String addressDetail;

}

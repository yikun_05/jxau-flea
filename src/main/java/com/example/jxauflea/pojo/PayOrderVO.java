package com.example.jxauflea.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PayOrderVO {

    private String trade_no;// 支付宝生成的订单号
    private String out_trade_no;// 自己生成的商户订单号
    private Double total_amount;// 订单总金额
    private Integer quantity;
    private Double price;
    private String userId;
    private String goodsId;
    private Date createTime;// 订单生成时间
    private String time;

    private String goodsName;
    private String description;
    private String type;
    private String userName;
    private String userImg;
    private List<String> imgUrls;

}

package com.example.jxauflea.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BoardPO {

    private String userId;
    private String userName;
    private String uuid;
    private String userImg;
    private String comment;
    private Date createTime;
    private String time;

}

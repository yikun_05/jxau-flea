package com.example.jxauflea.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderPO {


        private String uuid;
        private String userIdBuy;
        private String userIdSell;
        private String goodsId;
        private Date createTime;
}

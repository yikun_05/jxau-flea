package com.example.jxauflea.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentPO {

    private String uuid;
    private String goodsId;//1
    //判断是否为卖家
    private boolean isSeller;
    //评论用户名称
    private String userName;// 1
    //用户头像
    private String userImg;//1
    //被回复的用户的名称
    private String called_userName;//1
    private String content;// 评论内容 1
    private String parent_uuid;//父评论的uuid,为-1则说明该评论为顶级评论 1
    private String userId;//所属用户的uuid 1
    private Date createTime;
    private String[] activeNames = {"1"};
    //用于储存该评论的所有回复，该字段无需加入到数据库中
    private List<CommentPO> replyComment = new ArrayList<>();
    private String time;

}

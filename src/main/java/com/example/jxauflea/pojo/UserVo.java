package com.example.jxauflea.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserVo {

    // 用户ID
    private String uuid;
    // 用户名
    private String userName;
    // 状态（权限）
    private int state;
    // 头像
    private String imgUrl;
}

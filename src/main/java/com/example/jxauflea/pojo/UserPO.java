package com.example.jxauflea.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserPO {

        /** 用户ID*/
        private String userId;
        /** 用户名 */
        private String userName;
        /** 收件人名称 */
        private String rname;
        /** 密码 */
        private String password;
        /** 修改时间 */
        private Date modifiedTime;
        /** 状态（权限）*/
        private int state;
        /** 电话 */
        private String phone;
        /** 地址 */
        private String address;
        /** 是否打开浏览历史 */
        private int historyOpen;
        /** 性别 */
        private String gender;
        /** qq号 */
        private String qqNumber;
        /** 邮箱 */
        private String email;
        /** 头像 */
        private String imgUrl;

        private boolean historyOpenBool;

        private Address addressInfo  = new Address();

}

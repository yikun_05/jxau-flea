package com.example.jxauflea.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShopCarVO {


    private String uuid;
    //private String goodsId;
    private String goodsName;
    private double price;
    private String userId;
    private String description;
    private String type;
    private Date createTime;
    private String time;
    private String imgUrl;
    // 购物车商品数量
    private Integer quantity;
    // 是否被选中
    private boolean checked;
    private String userImg;
    private String userName;
}

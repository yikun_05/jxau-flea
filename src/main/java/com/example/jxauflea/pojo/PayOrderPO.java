package com.example.jxauflea.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PayOrderPO {

       private String trade_no;// 支付宝生成的订单号
       private String out_trade_no;// 自己生成的商户订单号
       private Double total_amount;// 订单总金额
       private Integer quantity;
       private Double price;
       private String userId;
       private String goodsId;
       private Date createTime;

}

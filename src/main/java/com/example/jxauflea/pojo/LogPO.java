package com.example.jxauflea.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogPO {

      private String uuid;// 日志编号
      private String logType;// 操作日志的类型
      private String logErrorCode;// 错误码
      private String logContent;// 操作日志的内容
      private String userId;// 用户id
      private Date logDate;// 日志创建时间
      private String userName;// 日志操作人
      private String ip;// 操作人的ip地址


}

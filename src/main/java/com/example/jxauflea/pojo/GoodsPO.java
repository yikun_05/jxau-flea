package com.example.jxauflea.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoodsPO {

        private String uuid;
        private String goodsName;
        private double price;
        private String userId;
        private String description;
        private Date modifiedTime;
        private String type;
        private Integer state;
        private Integer quantity;
        private Date createTime;


}

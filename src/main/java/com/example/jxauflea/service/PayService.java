package com.example.jxauflea.service;

import com.example.jxauflea.pojo.PayOrderPO;
import com.example.jxauflea.pojo.PayOrderVO;

import java.util.List;

public interface PayService {
    void userPay(PayOrderPO payOrderPO);

    List<PayOrderVO> getPayOrder(String userId);

    void deletePayOrder(PayOrderPO payOrderPO);
}

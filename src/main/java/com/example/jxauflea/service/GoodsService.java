package com.example.jxauflea.service;

import com.example.jxauflea.pojo.*;

import java.util.List;

public interface GoodsService {
    void releaseGoods(GoodsPO goodsPO, List<GoodsImgPO> list,OrderPO orderPO);

    void saveGoodsComment(CommentPO commentPO);

    void saveCollection(CollectionPO collectionPO);

    void deleteCollection(CollectionPO collectionPO);

    GoodsVO getGoodsDetail(String goodsId, String userId);

    void updateGoods(GoodsPO goodsPO);

    void updateGoodsImg(List<GoodsImgPO> list, String goodsId);

    List<GoodsVO> getUserCollection(String userId);

    List<GoodsVO> getUserPublish(String userId);

    List<ShopCarVO> getUserShopCar(String userId);

    void saveUserShopCar(ShopCarPO shopCarPO);

    List<GoodsVO> getUserViewHistory(String userId);

    void deleteHistory(HistoryPO history);

    void soldOutGood(GoodsPO goodsPO);

    List<HistoryVO> getUserHistoryVO(String userId);
}

package com.example.jxauflea.service;

import com.example.jxauflea.pojo.CommentPO;

import java.util.List;

public interface CommentService {

    List<CommentPO> findSonsByParentId(String commentId);

    List<CommentPO> listTop(String goodsId);

    void addComment(CommentPO commentPO);

    boolean judge(CommentPO commentPO);

}

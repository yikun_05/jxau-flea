package com.example.jxauflea.service;

import com.example.jxauflea.pojo.GoodsListVO;
import com.example.jxauflea.pojo.UserPO;
import com.example.jxauflea.pojo.UserVo;

import java.util.List;

public interface ManagerService {

    List<UserVo> listAllByRoot();

    List<UserVo> listAllByAdmin();

    void Ban(String uuid);

    void Unseal(String uuid);

    //商品通过审核
    void adopt(String goodsUuid);

    UserPO getDetail(String uuid);

    String appointAdmin(String uuid);

    //审核商品
    List<GoodsListVO> managerGetGoodsList();

}

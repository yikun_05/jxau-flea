package com.example.jxauflea.service;

import com.example.jxauflea.pojo.GoodsVO;
import com.example.jxauflea.pojo.OrderPO;

import java.util.List;

public interface OrderService {
    List<GoodsVO> getUserSell(String userId);

    void deleteUserSell(String userId);
}

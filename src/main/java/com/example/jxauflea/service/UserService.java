package com.example.jxauflea.service;

import com.example.jxauflea.pojo.UserPO;
import com.example.jxauflea.util.ResultEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;

public interface UserService {
    ResultEntity<Object> userLogin(String username, String password);

    ResultEntity<Object> userAdd(String username, String password);

    ResultEntity<String> userSendMessage(String phone);

    ResultEntity<String> verifySMS(String phone, String code);

    ResultEntity<String> updatePassword(String phone,String password);

    ResultEntity<String> updateUser(HashMap<String, String> userMap, MultipartFile file,boolean flag);

    ResultEntity<UserPO> getOneUser(String userId);
}

package com.example.jxauflea.service;

import com.example.jxauflea.pojo.LogPO;

import java.util.List;

public interface LogService {
    void saveInsertLog(LogPO log);

    List<LogPO> getAllLogs();

    void deleteLogById(String logId);

    List<LogPO> searchByContent(String content);
}

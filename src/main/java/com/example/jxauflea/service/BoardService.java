package com.example.jxauflea.service;

import com.example.jxauflea.pojo.BoardPO;

import java.util.List;

public interface BoardService {

    List<BoardPO> getAll();

    void saveMessage(BoardPO boardPO);
}

package com.example.jxauflea.service;

import com.example.jxauflea.pojo.GoodsVO;

import java.util.List;

public interface SearchService {

    List<GoodsVO> selectByKey(String goodsName,int state,int min,int max);

    List<String> selectImgByGoodsId(String goodsId);

    List<GoodsVO> selectByType(String type,int state,int min,int max);

    List<GoodsVO> selectByLove(int state);

}

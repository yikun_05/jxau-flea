package com.example.jxauflea.service.impl;

import com.example.jxauflea.mapper.ManagerMapper;
import com.example.jxauflea.pojo.GoodsListVO;
import com.example.jxauflea.pojo.UserPO;
import com.example.jxauflea.pojo.UserVo;
import com.example.jxauflea.service.ManagerService;
import com.example.jxauflea.service.StartingService;
import com.example.jxauflea.util.ResultEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ManagerServiceImpl implements ManagerService {

    @Autowired
    private ManagerMapper managerMapper;

    @Autowired
    private StartingService startingService;

    @Override
    public List<UserVo> listAllByRoot() {
        return managerMapper.listAllByRoot();
    }

    @Override
    public List<UserVo> listAllByAdmin() {
        return managerMapper.listAllByRoot();
    }

    @Override
    public void Ban(String uuid) {
        managerMapper.Ban(uuid);
    }

    @Override
    public void Unseal(String uuid) {
        managerMapper.Unseal(uuid);
    }

    @Override
    public void adopt(String goodsUuid) {
        managerMapper.adopt(goodsUuid);
    }

    @Override
    public UserPO getDetail(String uuid) {
        return managerMapper.getDetail(uuid);
    }

    @Override
    public String appointAdmin(String uuid) {
        managerMapper.appointAdmin(uuid);
        return "任命成功!";
    }

    @Override
    public List<GoodsListVO> managerGetGoodsList() {
        List<GoodsListVO> goodsListVOS = managerMapper.managerGetGoodsList();

        for (GoodsListVO goodsListVO : goodsListVOS) {
            List<String> strings=startingService.getGoodsImgUrl(goodsListVO.getUuid());
            if(!strings.isEmpty())
            goodsListVO.setGoodsImgUrl(strings.get(0));
        }
        return goodsListVOS;
    }
}

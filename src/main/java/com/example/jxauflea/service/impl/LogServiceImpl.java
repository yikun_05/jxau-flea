package com.example.jxauflea.service.impl;

import com.example.jxauflea.mapper.LogMapper;
import com.example.jxauflea.pojo.LogPO;
import com.example.jxauflea.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogServiceImpl implements LogService {

    @Autowired
    private LogMapper logMapper;

    @Override
    public void saveInsertLog(LogPO log) {

        logMapper.insertLog(log);
    }

    @Override
    public List<LogPO> getAllLogs() {

        List<LogPO> list=logMapper.selectAll();
        return list;
    }

    @Override
    public void deleteLogById(String logId) {
        logMapper.deleteLogById(logId);
    }

    @Override
    public List<LogPO> searchByContent(String content) {
        return logMapper.selectByContent(content);
    }
}

package com.example.jxauflea.service.impl;

import com.example.jxauflea.mapper.BoardMapper;
import com.example.jxauflea.pojo.BoardPO;
import com.example.jxauflea.service.BoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class BoardServiceImpl implements BoardService {

    @Autowired
    private BoardMapper boardMapper;

    @Override
    @Transactional(readOnly = true)
    public List<BoardPO> getAll() {

        List<BoardPO> boardPOS = boardMapper.selectAll();
        for (BoardPO boardPO : boardPOS) {
            init(boardPO);
        }
        return boardPOS;


    }

    @Override
    public void saveMessage(BoardPO boardPO) {
        boardMapper.addMessage(boardPO);
    }

    public void init(BoardPO boardPO) {
        Date createTime = boardPO.getCreateTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = simpleDateFormat.format(createTime);
        boardPO.setTime(format);
    }
}

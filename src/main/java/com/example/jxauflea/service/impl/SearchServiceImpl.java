package com.example.jxauflea.service.impl;

import com.example.jxauflea.mapper.SearchMapper;
import com.example.jxauflea.pojo.GoodsVO;
import com.example.jxauflea.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    private SearchMapper searchMapper;

    @Override
    public List<GoodsVO> selectByKey(String goodsName,int state,int min,int max) {
        return searchMapper.selectByKey("%"+goodsName+"%",state,min,max);//在这里实现模糊查询
    }

    @Override
    public List<String> selectImgByGoodsId(String goodsId) {
        return searchMapper.selectImgByGoodsId(goodsId);
    }

    @Override
    public List<GoodsVO> selectByType(String type, int state, int min, int max) {
        return searchMapper.selectByType(type, state, min, max);
    }

    @Override
    public List<GoodsVO> selectByLove(int state) {
        return searchMapper.selectByLove(state);
    }
}

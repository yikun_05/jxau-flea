package com.example.jxauflea.service.impl;

import com.example.jxauflea.mapper.OrderMapper;
import com.example.jxauflea.pojo.GoodsVO;
import com.example.jxauflea.pojo.OrderPO;
import com.example.jxauflea.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {


    @Autowired
    private OrderMapper orderMapper;

    @Override
    public List<GoodsVO> getUserSell(String userId) {
        return orderMapper.selectUserSell(userId);
    }

    @Override
    public void deleteUserSell(String userId) {
        orderMapper.deleteUserSell(userId);
    }
}

package com.example.jxauflea.service.impl;

import com.example.jxauflea.mapper.CommentMapper;
import com.example.jxauflea.pojo.CommentPO;
import com.example.jxauflea.service.CommentService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentMapper commentMapper;

    @Override
    public List<CommentPO> findSonsByParentId(String commentId) {
        return commentMapper.findSonsByParentId(commentId);
    }

    @Override
    public List<CommentPO> listTop(String goodsId) {
                                                 //找到所有的根节点
        List<CommentPO> comments = commentMapper.findByCommentIdAndParentCommentNull(goodsId);
        return eachComment(comments);
    }

    @Override
    public void addComment(CommentPO commentPO) {
        commentMapper.addComment(commentPO);
    }

    @Override
    public boolean judge(CommentPO commentPO) {
        return commentMapper.judge(commentPO) > 0;
    }


    private List<CommentPO> eachComment(List<CommentPO> comments) {
        //复制出一个新的list集合,避免对原有数据进行修改
        List<CommentPO> commentsViews = new ArrayList<>();
        for (CommentPO comment : comments) {
            CommentPO c = new CommentPO();
            BeanUtils.copyProperties(comment,c);
            init(c);
            commentsViews.add(c);
        }
        //合并集合的各层到第一级的子代集合中，这里传输的是顶级评论集合
        combineChildren(commentsViews);
        return commentsViews;
    }


    //总共只有两级，根节点，以及它的所有子代

    // 存放所有子代的集合
    private List<CommentPO> tempReplies = new ArrayList<>();

    public void init(CommentPO commentPO) {
        Date createTime = commentPO.getCreateTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format = simpleDateFormat.format(createTime);
        commentPO.setTime(format);
    }


    /**
     *
     * @param comments root根节点，comment不为空的对象集合
     */
    private void combineChildren(List<CommentPO> comments) {
        //comment 是每一个顶级评论
        for (CommentPO comment : comments) {
            //每一个集合都获取自身所有的回复集合
            List<CommentPO> replies = commentMapper.findSonsByParentId(comment.getUuid());
            for(CommentPO reply : replies) {
                // 循环迭代。找出所有子代，存放在tempReplies中
                init(reply);
                recursively(reply);
            }
            //最终结果就 是找到并储存了这个根节点下面的所有节点集合

            //将这个结果集，设定了根节点（树的第一级）的新子集（即树的第二级）
            comment.setReplyComment(tempReplies);
            //清除临时存放区
            tempReplies = new ArrayList<>();
        }
    }


    //通过这个递归循环迭代，可以找出并储存所有的子集合
    private void recursively(CommentPO comment) {
        tempReplies.add(comment);//顶节点添加到临时存放集合中
        List<CommentPO> replies = commentMapper.findSonsByParentId(comment.getUuid());
        //如果子集合中有元素
        if (replies.size() > 0) {
            //那么就把这些元素全部放入tempReplies中
            for (CommentPO reply : replies) {
                init(reply);
                reply.setReplyComment(commentMapper.findSonsByParentId(reply.getUuid()));
                tempReplies.add(reply);
                //同上，直到子集合中没有元素，停止递归
                for (CommentPO commentPO : reply.getReplyComment()) {
                    recursively(commentPO);
                }
            }
        }
    }



}

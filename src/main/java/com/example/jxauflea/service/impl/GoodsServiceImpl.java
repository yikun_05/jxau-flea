package com.example.jxauflea.service.impl;

import com.example.jxauflea.mapper.*;
import com.example.jxauflea.pojo.*;
import com.example.jxauflea.service.GoodsService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service
@Transactional
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private GoodsImgMapper goodsImgMapper;

    @Autowired
    private StartingMapper startingMapper;

    @Autowired
    private CommentMapper commentMapper;

    @Autowired
    private CollectionMapper collectionMapper;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private HistoryMapper historyMapper;

    @Autowired
    private ShopCarMapper shopCarMapper;

    @Autowired
    private UserMapper userMapper;

    @Override
    public void releaseGoods(GoodsPO goodsPO, List<GoodsImgPO> list, OrderPO orderPO) {

        goodsMapper.saveGoods(goodsPO);// 保存商品信息，保存失败，回滚
        if(list!=null&&!list.isEmpty())
        goodsImgMapper.saveGoodsImg(list);// 保存商品图片
        orderMapper.saveOrderUserSell(orderPO);// 订单要增加一条信息


    }

    @Override
    public void saveGoodsComment(CommentPO commentPO) {
        commentMapper.addComment(commentPO);
    }

    @Override
    public void saveCollection(CollectionPO collectionPO) {
        collectionMapper.addCollection(collectionPO);
    }

    @Override
    public void deleteCollection(CollectionPO collectionPO) {
        collectionMapper.deleteCollection(collectionPO);
    }

    @Override
    public GoodsVO getGoodsDetail(String goodsId, String userId) {

        // 根据id查询单条数据
        //GoodsPO goodsPO= goodsMapper.setlectById(goodsId);
        GoodsVO goodsVO1=goodsMapper.selectGoodsVOById(goodsId);

        Date createTime = goodsVO1.getCreateTime();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");  // 设置日期格式
        String format = simpleDateFormat.format(createTime);
        goodsVO1.setTime(format);

        // 查询出商品图片信息
        List<String> list= goodsImgMapper.selectByGoodsId(goodsId);
        //GoodsVO goodsVO=new GoodsVO();
        //BeanUtils.copyProperties(goodsPO,goodsVO);// 相同属性拷贝
        goodsVO1.setImgUrls(list);

        // 如果user不为空，并且浏览历史中没有对应的数据，增加浏览历史
        //UserPO userPO = userMapper.selectOneUser(userId);
        UserPO userPO =userMapper.selectUser(userId);
        if(userPO!=null){

            HistoryPO historyPO=new HistoryPO();
            historyPO.setUserId(userId);
            historyPO.setGoodsId(goodsId);
            HistoryPO historyPO1 = historyMapper.selectHistoryByKeys(historyPO);
            if(historyPO1==null){
                historyMapper.insertHistory(historyPO);
            }else{
                // 更新浏览时间
                historyMapper.updateHistory(historyPO);
            }

        }

        // 判断该用户是否收藏过该商品
        CollectionPO collectionPO=collectionMapper.selectPO(userId,goodsId);
        if(collectionPO!=null){
            // 不为空代表用户收藏过该商品
            goodsVO1.setIsCollected(true);
        }else{
            goodsVO1.setIsCollected(false);
        }

        return goodsVO1;
    }

    @Override
    public void updateGoods(GoodsPO goodsPO) {
        goodsMapper.updateGoods(goodsPO);
    }

    @Override
    public void updateGoodsImg(List<GoodsImgPO> list, String goodsId) {
        goodsImgMapper.updateGoodsImg(list,goodsId);
    }

    @Override
    public List<GoodsVO> getUserCollection(String userId) {

        List<GoodsVO> list=collectionMapper.selectGoodsVoByUserId(userId);
        for (GoodsVO goodsVO : list) {
            // 循环查询出商品图品并进行赋值操作
            List<String> imgUrlList = goodsImgMapper.selectByGoodsId(goodsVO.getUuid());// 返回一个图片集合但是可以选择一张作为展示图片
            goodsVO.setImgUrls(imgUrlList);
        }
        return list;
    }

    @Override
    public List<GoodsVO> getUserPublish(String userId) {

        List<GoodsPO> list=goodsMapper.selectByUserId(userId);
        List<GoodsVO> goodsVOList=new LinkedList<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        for (GoodsPO goodsPO : list) {
            // 循环查询出商品图品并进行赋值操作
            GoodsVO goodsVO =new GoodsVO();
            BeanUtils.copyProperties(goodsPO,goodsVO);// 可能顺序会反
            List<String> imgUrlList = goodsImgMapper.selectByGoodsId(goodsVO.getUuid());// 返回一个图片集合但是可以选择一张作为展示图片
            goodsVO.setImgUrls(imgUrlList);
            goodsVOList.add(goodsVO);
            // 更改时间格式
            Date createTime = goodsVO.getCreateTime();
            String format = simpleDateFormat.format(createTime);
            goodsVO.setTime(format);
        }
        return goodsVOList;
    }

    @Override
    public List<ShopCarVO> getUserShopCar(String userId) {

        List<ShopCarVO> list=shopCarMapper.selectUserShopCar(userId);
        for (ShopCarVO shopCarVO : list) {
            // 查询出的图片集合不为空才设置
            List<String> goodsImgUrl1 = startingMapper.getGoodsImgUrl(shopCarVO.getUuid());
            if(goodsImgUrl1!=null&goodsImgUrl1.size()!=0){
                String goodsImgUrl = goodsImgUrl1.get(0);
                shopCarVO.setImgUrl(goodsImgUrl);
            }
            shopCarVO.setChecked(false);
        }

        return list;

    }

    @Override
    public void saveUserShopCar(ShopCarPO shopCarPO) {

        // 先根据goodsId查询记录，如果为空，创建记录，商品数量设置为0

        ShopCarPO shopCarPO1= shopCarMapper.selectShopCarByPO(shopCarPO);
        if(shopCarPO1==null){
            shopCarPO.setQuantity(1);
            shopCarMapper.addShopCar(shopCarPO);
        }else{
            // 不为空，更改数量加一
            Integer count = shopCarPO1.getQuantity();
            shopCarPO.setQuantity(++count);
            shopCarMapper.updateShopCarCount(shopCarPO);
        }

    }

    @Override
    public List<HistoryVO> getUserHistoryVO(String userId) {
        List<HistoryVO> list=historyMapper.selectHistoryVOByUserId(userId);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        for (HistoryVO goodsVO : list) {
            // 循环查询出商品图品并进行赋值操作
            List<String> imgUrlList = goodsImgMapper.selectByGoodsId(goodsVO.getUuid());// 返回一个图片集合但是可以选择一张作为展示图片
            goodsVO.setImgUrls(imgUrlList);
            Date createTime = goodsVO.getCreateTime();
            // 更改时间格式
            String format = simpleDateFormat.format(createTime);
            goodsVO.setTime(format);

        }
        return list;
    }

    @Override
    public List<GoodsVO> getUserViewHistory(String userId) {
        List<GoodsVO> list=historyMapper.selectByUserId(userId);
        for (GoodsVO goodsVO : list) {
            // 循环查询出商品图品并进行赋值操作
            List<String> imgUrlList = goodsImgMapper.selectByGoodsId(goodsVO.getUuid());// 返回一个图片集合但是可以选择一张作为展示图片
            goodsVO.setImgUrls(imgUrlList);
        }
        return list;
    }

    @Override
    public void deleteHistory(HistoryPO history) {

        historyMapper.deletePO(history);

    }

    @Transactional
    @Override
    public void soldOutGood(GoodsPO goodsPO) {

        // 删除商品表数据

        // 删除商品图库表数据

        // 删除收藏表数据

        // 删除购物车表数据

        // 删除浏览历史表数据

        goodsMapper.deletePO(goodsPO);
    }

    public void init(CommentPO commentPO) {
        Date createTime = commentPO.getCreateTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format = simpleDateFormat.format(createTime);
        commentPO.setTime(format);
    }
}

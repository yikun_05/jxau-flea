package com.example.jxauflea.service.impl;

import com.example.jxauflea.mapper.StartingMapper;
import com.example.jxauflea.pojo.GoodsPO;
import com.example.jxauflea.pojo.StartVo;
import com.example.jxauflea.service.StartingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class StartingServiceImpl implements StartingService {

    @Autowired
    private StartingMapper startingMapper;


    @Override
    public List<StartVo> getGoodsAndUserDetail() {
        return startingMapper.getGoodsAndUserDetail();
    }

    @Override
    public List<String> getGoodsImgUrl(String goodsId) {
        return startingMapper.getGoodsImgUrl(goodsId);
    }

    @Override
    public List<StartVo> Starting() {
        List<StartVo> list = getGoodsAndUserDetail();
        for (StartVo s:
             list) {
            Date createTime = s.getCreateTime();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String format = simpleDateFormat.format(createTime);
            s.setTime(format);
            s.setImgUrls(getGoodsImgUrl(s.getUuid()));
        }

        return list;
    }

}

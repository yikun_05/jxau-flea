package com.example.jxauflea.service;

import com.example.jxauflea.pojo.StartVo;

import java.util.List;

public interface StartingService {

    //获取五条商品信息
    List<StartVo> getGoodsAndUserDetail();

    //获取商品图片的地址
    List<String> getGoodsImgUrl(String goodsId);

    List<StartVo> Starting();
}

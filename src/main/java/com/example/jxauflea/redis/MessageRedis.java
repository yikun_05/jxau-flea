package com.example.jxauflea.redis;

import com.example.jxauflea.pojo.PayOrderPO;
import com.example.jxauflea.util.ResultEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import redis.clients.jedis.Jedis;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

public class MessageRedis {

    private static Jedis jedis= new Jedis("127.0.0.1",6379);

    public ResultEntity<String> addOneMessage(String phone,String code){
        try{
            boolean keyExist = jedis.exists(phone);
            // NX是不存在时才set， XX是存在时才set， EX是秒，PX是毫秒
            if (keyExist) {
                jedis.del(phone);
            }
                jedis.set(phone, code);
                jedis.expire(phone,300);
            return ResultEntity.successWithoutData();
        }catch (Exception e){
            e.printStackTrace();
            return ResultEntity.falseWithoutData("error");
        }
    }

    public ResultEntity<String> deleteOneMessage(String phone, String code) {
        try{
            boolean keyExist = jedis.exists(phone);
            if (!keyExist) {
                return ResultEntity.falseWithoutData("请先发送验证码");
            }else {
                if (code.equals(jedis.get(phone))){
                    jedis.del(phone);
                    return ResultEntity.successWithoutData();
                }else {
                    return ResultEntity.falseWithoutData("验证码错误");
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
       return ResultEntity.falseWithoutData("error");
    }


}

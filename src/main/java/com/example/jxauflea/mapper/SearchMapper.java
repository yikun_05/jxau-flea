package com.example.jxauflea.mapper;

import com.example.jxauflea.pojo.GoodsVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


public interface SearchMapper {

    List<GoodsVO> selectByKey(String goodsName,int state,int min,int max);

    List<String> selectImgByGoodsId(String goodsId);

    List<GoodsVO> selectByType(String type,int state,int min,int max);

    List<GoodsVO> selectByLove(int state);


}

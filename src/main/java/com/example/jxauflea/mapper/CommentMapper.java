package com.example.jxauflea.mapper;


import com.example.jxauflea.pojo.CommentPO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


public interface CommentMapper {
                                        //根据评论的uuid来查询其所属
    List<CommentPO> findSonsByParentId(String commentId);

    CommentPO getByCommentId(String commentId);

    List<CommentPO> findByCommentIdAndParentCommentNull(String goodsId);

    void addComment(CommentPO commentPO);

    /*judge
    判断该评论是否出自卖家
    根据CommentPO的goodsId找到该商品
    然后比较CommentPO的userId值是否等于goods表中的userId值
    若返回值大于0就说明是，0则不是
     */

    int judge(CommentPO commentPO);

}

package com.example.jxauflea.mapper;

import com.example.jxauflea.pojo.StartVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


public interface StartingMapper {

    //获取五条商品信息
    List<StartVo> getGoodsAndUserDetail();

    //获取商品图片的地址
    List<String> getGoodsImgUrl(String goodsId);

}

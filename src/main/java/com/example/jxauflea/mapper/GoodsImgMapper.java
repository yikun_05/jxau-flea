package com.example.jxauflea.mapper;

import com.example.jxauflea.pojo.GoodsImgPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface GoodsImgMapper {


    void saveGoodsImg(List<GoodsImgPO> list);

    List<String> selectByGoodsId(String goodsId);

    void updateGoodsImg(@Param("list") List<GoodsImgPO> list, @Param("goodsId")  String goodsId);
}
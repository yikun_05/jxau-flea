package com.example.jxauflea.mapper;


import com.example.jxauflea.pojo.PayOrderPO;
import com.example.jxauflea.pojo.PayOrderVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


public interface PayOrderMapper {

    void insertPayOrderPO(PayOrderPO payOrderPO);

    List<PayOrderVO> selectByUserId(String userId);

    void deletePayOrder(PayOrderPO payOrderPO);
}

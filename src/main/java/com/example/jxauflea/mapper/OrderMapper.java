package com.example.jxauflea.mapper;

import com.example.jxauflea.pojo.GoodsVO;
import com.example.jxauflea.pojo.OrderPO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


public interface OrderMapper {

    void saveOrderUserSell(OrderPO orderPO);

    List<GoodsVO> selectUserSell(String userId);

    void deleteUserSell(String userId);

    void indesrtBuyOrderPO(OrderPO orderPO);
}

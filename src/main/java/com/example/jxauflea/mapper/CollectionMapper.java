package com.example.jxauflea.mapper;

import com.example.jxauflea.pojo.CollectionPO;
import com.example.jxauflea.pojo.GoodsPO;
import com.example.jxauflea.pojo.GoodsVO;
import com.example.jxauflea.pojo.ShopCarVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface CollectionMapper {


    void addCollection(CollectionPO collectionPO);

    void deleteCollection(CollectionPO collectionPO);

    List<CollectionPO> selectByUserId(String userId);

    List<GoodsVO> selectGoodsByUserId(String userId);

    List<ShopCarVO> selectShopCarVO(String userId);

    CollectionPO selectPO(@Param("userId") String userId,@Param("goodsId") String goodsId);

    List<GoodsVO> selectGoodsVoByUserId(String userId);
}

package com.example.jxauflea.mapper;

import com.example.jxauflea.pojo.GoodsPO;
import com.example.jxauflea.pojo.GoodsVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


public interface GoodsMapper {

    void saveGoods(GoodsPO goodsPO);

    GoodsPO setlectById(String goodsId);

    void updateGoods(GoodsPO goodsPO);

    String getUserId(String goodsId);

    List<GoodsPO> selectByUserId(String userId);

    GoodsVO selectGoodsVOById(String goodsId);

    void deletePO(GoodsPO goodsPO);

    void soldGood(String goodsId);
}

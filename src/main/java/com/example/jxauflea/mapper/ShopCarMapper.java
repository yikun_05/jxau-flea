package com.example.jxauflea.mapper;

import com.example.jxauflea.pojo.BoardPO;
import com.example.jxauflea.pojo.ShopCarPO;
import com.example.jxauflea.pojo.ShopCarVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface ShopCarMapper {


    void cleanByUserIdAndGoodId(@Param("userId") String userId,@Param("goodsId") String goodsId);

    List<ShopCarVO> selectUserShopCar(String userId);

    void addShopCar(ShopCarPO shopCarPO);

    void updateShopCarCount(ShopCarPO shopCarPO);


    ShopCarPO selectShopCarByPO(ShopCarPO shopCarPO);
}

package com.example.jxauflea.mapper;

import com.example.jxauflea.pojo.GoodsPO;
import com.example.jxauflea.pojo.GoodsVO;
import com.example.jxauflea.pojo.HistoryPO;
import com.example.jxauflea.pojo.HistoryVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


public interface HistoryMapper {


    void insertHistory(HistoryPO historyPO);

    HistoryPO selectHistoryByKeys(HistoryPO historyPO);

    List<GoodsVO> selectByUserId(String userId);

    void deletePO(HistoryPO history);

    void updateHistory(HistoryPO historyPO);

    List<HistoryVO> selectHistoryVOByUserId(String userId);
}

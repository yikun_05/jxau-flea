package com.example.jxauflea.mapper;

import com.example.jxauflea.pojo.GoodsListVO;
import com.example.jxauflea.pojo.UserPO;
import com.example.jxauflea.pojo.UserVo;

import java.util.List;

public interface ManagerMapper {

    //root用户获取用户所有信息
    List<UserVo> listAllByRoot();
    //admin用户获取用户所有信息
    List<UserVo> listAllByAdmin();
    //封禁用户
    void Ban(String uuid);
    //解封用户
    void Unseal(String uuid);
    //获取用户详情
    UserPO getDetail(String uuid);
    //任命管理员
    void appointAdmin(String uuid);

    //商品通过审核
    void adopt(String goodsUuid);

    //审核商品
    List<GoodsListVO> managerGetGoodsList();


}

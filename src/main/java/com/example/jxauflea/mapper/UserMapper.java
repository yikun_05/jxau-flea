package com.example.jxauflea.mapper;

import com.example.jxauflea.pojo.UserPO;
import com.example.jxauflea.util.ResultEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMapper {

    // 用户登入方法
    UserPO loginOneUser(@Param("username") String username,@Param("password") String password);

    // 用户注册方法
    int addUser(UserPO userPO);

    // 用户修改密码方法
    int updatePassWord(String phone,String password);

    // 用户修改信息方法
    int updateUser(UserPO userPO);

    UserPO selectOneUser(String userId);

    UserPO selectUser(String userId);
}

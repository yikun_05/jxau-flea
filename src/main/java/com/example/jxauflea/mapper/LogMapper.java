package com.example.jxauflea.mapper;

import com.example.jxauflea.pojo.LogPO;

import java.util.List;

public interface LogMapper {

    void insertLog(LogPO log);

    List<LogPO> selectAll();

    void deleteLogById(String logId);

    List<LogPO> selectByContent(String content);
}

package com.example.jxauflea.mapper;

import com.example.jxauflea.pojo.BoardPO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


public interface BoardMapper {


    List<BoardPO> selectAll();

    void addMessage(BoardPO boardPO);
}

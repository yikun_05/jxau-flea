package com.example.jxauflea;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.example.jxauflea.mapper")
@SpringBootApplication
public class JxauFleaApplication {

    public static void main(String[] args) {
        SpringApplication.run(JxauFleaApplication.class, args);
    }

}

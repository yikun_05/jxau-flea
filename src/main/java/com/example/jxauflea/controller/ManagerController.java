package com.example.jxauflea.controller;

import com.example.jxauflea.pojo.GoodsListVO;
import com.example.jxauflea.pojo.UserPO;
import com.example.jxauflea.pojo.UserVo;
import com.example.jxauflea.service.ManagerService;
import com.example.jxauflea.util.ResultEntity;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
public class ManagerController {

    @Autowired
    private ManagerService managerService;

    @ApiOperation(value = "root用户获取所有用户信息（包括管理员）", notes ="无需参数")
    @PostMapping("/manager/list/allByRoot")
    public ResultEntity<List<UserVo>> listAllUserByRoot() {
        return ResultEntity.successWithData(managerService.listAllByRoot());
    }

    @ApiOperation(value = "admin用户获取所有用户信息（不包括管理员）", notes ="无需参数")
    @PostMapping("/manager/list/allByAdmin")
    public ResultEntity<List<UserVo>> listAllUserByAdmin() {
        return ResultEntity.successWithData(managerService.listAllByAdmin());
    }

    @ApiOperation(value = "封禁用户", notes ="需要该用户的uuid")
    @PostMapping("/manager/ban")
    public ResultEntity Ban(@RequestParam("userId")String userId) {
        managerService.Ban(userId);
        return ResultEntity.successWithData("该用户封禁成功!");
    }

    @ApiOperation(value = "解封用户", notes ="需要该用户的uuid")
    @PostMapping("/manager/unseal")
    public ResultEntity Unseal(@RequestParam("userId")String userId) {
        managerService.Unseal(userId);
        return ResultEntity.successWithData("该用户解封成功!");
    }


    @ApiOperation(value = "查看该用户的详情", notes ="需要该用户的uuid")
    @PostMapping("/manager/getDetail")
    public ResultEntity<UserPO> getDetail(@RequestBody HashMap<String,String> hashMap) {
        return ResultEntity.successWithData(managerService.getDetail(hashMap.get("uuid")));
    }

    @ApiOperation(value = "任命为管理员", notes ="需要该用户的uuid")
    @PostMapping("/manager/appointAdmin")
    public ResultEntity<String> appointAdmin(@RequestParam("userId")String userId) {
        return ResultEntity.successWithData(managerService.appointAdmin(userId));
    }

    @ApiOperation(value = "审核商品", notes ="无需参数")
    @PostMapping("/manager/check")
    public ResultEntity<List<GoodsListVO>> check() {
        return ResultEntity.successWithData(managerService.managerGetGoodsList());
    }

    @ApiOperation(value = "商品通过审核", notes ="该商品的uuid")
    @PostMapping("/manager/adopt")
    public ResultEntity<String> adopt(@RequestParam("goodsUuid")String goodsUuid) {
        managerService.adopt(goodsUuid);
        return ResultEntity.successWithData("商品"+goodsUuid+"已通过审核!");
    }




}

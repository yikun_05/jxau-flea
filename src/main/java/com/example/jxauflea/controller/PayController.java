package com.example.jxauflea.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.example.jxauflea.config.PayProperties;
import com.example.jxauflea.pojo.PayOrderPO;
import com.example.jxauflea.pojo.PayOrderVO;
import com.example.jxauflea.redis.MessageRedis;
import com.example.jxauflea.service.PayService;

import com.example.jxauflea.util.ResultEntity;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.transform.Result;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class PayController {

    @Autowired
    private PayProperties payProperties;

    @Autowired
    private PayService payService;

    private static PayOrderPO payOrderPO;


    @RequestMapping("/pay")
    public void payController(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws IOException {
        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(payProperties.getGatewayUrl(),
                payProperties.getAppId(),
                payProperties.getMerchantPrivateKey(),
                "json",
                payProperties.getCharset(),
                payProperties.getAlipayPublicKey(),
                payProperties.getSignType());

        //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(payProperties.getReturnUrl());
        alipayRequest.setNotifyUrl(payProperties.getNotifyurl());


        // 生成商户订单号（当前时间+UUID）
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyyMMddHHmmss");
        Date date=new Date();
        String format = simpleDateFormat.format(date);
        String uuid = UUID.randomUUID().toString().replace("-", "").toUpperCase();
        String out_trade_no=format+uuid;
        //付款金额，必填
        String price=new String(request.getParameter("price").getBytes("ISO-8859-1"),"UTF-8");
        String quantity=new String(request.getParameter("quantity").getBytes("ISO-8859-1"),"UTF-8");
        //String total_amounth = new String(request.getParameter("WIDtotal_amount").getBytes("ISO-8859-1"),"UTF-8");
        Double total_amount=Double.parseDouble(price)*Integer.parseInt(quantity);
        //订单名称，必填
        String subject = new String(request.getParameter("WIDsubject"));
        //订单描述，或者备注可空
        String body = new String(request.getParameter("WIDbody"));

        // 封装实体类
        payOrderPO=new PayOrderPO();
        payOrderPO.setOut_trade_no(out_trade_no);
        payOrderPO.setPrice(Double.parseDouble(price));
        payOrderPO.setQuantity(Integer.parseInt(quantity));
        payOrderPO.setTotal_amount(total_amount);
        payOrderPO.setUserId(request.getParameter("userId"));
        payOrderPO.setGoodsId(request.getParameter("goodsId"));

        session.setAttribute("payOrderPO",payOrderPO);
        request.setAttribute("payOrderPO",payOrderPO);



        alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        //请求
        String form="";
        try {
            form = alipayClient.pageExecute(alipayRequest).getBody(); //调用SDK生成表单
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        response.setContentType("text/html;charset=" + payProperties.getCharset());
        response.getWriter().write(form);//直接将完整的表单html输出到页面
        response.getWriter().flush();
        response.getWriter().close();

    }

/*    @RequestMapping("/pay")
    public ResultEntity<String> payController(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws IOException {
        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient( payProperties.getGatewayUrl(),
                payProperties.getAppId(),
                payProperties.getMerchantPrivateKey(),
                "json",
                payProperties.getCharset(),
                payProperties.getAlipayPublicKey(),
                payProperties.getSignType());

        //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(payProperties.getReturnUrl());
        alipayRequest.setNotifyUrl(payProperties.getNotifyurl());


        // 生成商户订单号（当前时间+UUID）
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyyMMddHHmmss");
        Date date=new Date();
        String format = simpleDateFormat.format(date);
        String uuid = UUID.randomUUID().toString().replace("-", "").toUpperCase();
        String out_trade_no=format+uuid;
        //付款金额，必填
        String price=new String(request.getParameter("price").getBytes("ISO-8859-1"),"UTF-8");
        String quantity=new String(request.getParameter("quantity").getBytes("ISO-8859-1"),"UTF-8");
        //String total_amounth = new String(request.getParameter("WIDtotal_amount").getBytes("ISO-8859-1"),"UTF-8");
        Double total_amount=Double.parseDouble(price)*Integer.parseInt(quantity);
        //订单名称，必填
        String subject = new String(request.getParameter("WIDsubject").getBytes("ISO-8859-1"),"UTF-8");
        //订单描述，或者备注可空
        String body = new String(request.getParameter("WIDbody").getBytes("ISO-8859-1"),"UTF-8");

        // 封装实体类
        PayOrderPO payOrderPO=new PayOrderPO();
        payOrderPO.setOut_trade_no(out_trade_no);
        payOrderPO.setPrice(Double.parseDouble(price));
        payOrderPO.setQuantity(Integer.parseInt(quantity));
        payOrderPO.setTotal_amount(total_amount);
        payOrderPO.setUserId(request.getParameter("userId"));
        payOrderPO.setGoodsId(request.getParameter("goodsId"));

        session.setAttribute("payOrderPO",payOrderPO);

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        //请求
        String form="";
        try {
            form = alipayClient.pageExecute(alipayRequest).getBody(); //调用SDK生成表单
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        response.setContentType("text/html;charset=" + payProperties.getCharset());
        response.getWriter().write(form);//直接将完整的表单html输出到页面
        response.getWriter().flush();
        response.getWriter().close();
        return ResultEntity.successWithData(form);

    }*/


    @RequestMapping("/notify")
    public ResultEntity<String> doReturn(HttpServletRequest request, HttpSession session) throws UnsupportedEncodingException, AlipayApiException {

        // 用户浏览器发送过来的请求
        // 获取支付宝GET过来反馈信息
        Map<String,String> params = new HashMap<String,String>();
        Map<String,String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++)  {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用
            valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }

        boolean signVerified = AlipaySignature.rsaCheckV1(
                params,
                payProperties.getAlipayPublicKey(),
                payProperties.getCharset(),
                payProperties.getSignType()); //调用SDK验证签名

        //——请在这里编写您的程序（以下代码仅作参考）——
        if(signVerified) {
            //商户订单号
            String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"),"UTF-8");

            //支付宝交易号
            String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"),"UTF-8");

            //付款金额
            String total_amount = new String(request.getParameter("total_amount").getBytes("ISO-8859-1"),"UTF-8");


            // 存入数据库
            try{
                //PayOrderPO payOrderPO=(PayOrderPO) session.getAttribute("payOrderPO");
               // PayOrderPO payOrderPO=(PayOrderPO)request.getAttribute("payOrderPO");
                payOrderPO.setTrade_no(trade_no);
                payService.userPay(payOrderPO);

                return ResultEntity.successWithoutData();
            }catch (Exception e){
                return ResultEntity.falseWithoutData(e.getMessage());
            }


            // 把returnVo保存到数据库中
           /* OrderVO orderVO=(OrderVO)session.getAttribute("orderVO");
            orderVO.setPayOrderNum(trade_no);
            ResultEntity<String> resultEntity= mySqlRemoteService.saveOrderRemote(orderVO);*/


            //return "trade_no:"+trade_no+"<br/>out_trade_no:"+out_trade_no+"<br/>total_amount:"+total_amount;
        }else {
            return ResultEntity.falseWithoutData("验签失败");
        }
    }

    @RequestMapping("/return")
    public String doNotify(HttpServletRequest request) throws UnsupportedEncodingException, AlipayApiException {

        // 支付宝发送过来的请求
        // 获取支付宝POST过来反馈信息
        Map<String,String> params = new HashMap<String,String>();
        Map<String,String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用
            valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }

        boolean signVerified = AlipaySignature.rsaCheckV1(
                params,
                payProperties.getAlipayPublicKey(),
                payProperties.getCharset(),
                payProperties.getSignType()); //调用SDK验证签名

        //——请在这里编写您的程序（以下代码仅作参考）——

	/* 实际验证过程建议商户务必添加以下校验：
	1、需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
	2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
	3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
	4、验证app_id是否为该商户本身。
	*/
        if(signVerified) {//验证成功
            //商户订单号
            String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"),"UTF-8");

            //支付宝交易号
            String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"),"UTF-8");

            //交易状态
            String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"),"UTF-8");

            if(trade_status.equals("TRADE_FINISHED")){
                //判断该笔订单是否在商户网站中已经做过处理
                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                //如果有做过处理，不执行商户的业务程序

                //注意：
                //退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知
            }else if (trade_status.equals("TRADE_SUCCESS")){
                //判断该笔订单是否在商户网站中已经做过处理
                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                //如果有做过处理，不执行商户的业务程序

                //注意：
                //付款完成后，支付宝系统发送该交易状态通知
            }

        }
        return "redirect:http://10.100.16.171:8080/home";

    }


    @PostMapping("/pay/get/userorder")
    @ApiOperation(value = "用户查看自己支付成功的历史订单", notes ="需要接收用户的id")
    @ResponseBody
    public ResultEntity<List<PayOrderVO>> doGetUserOrder(@RequestBody Map<String,String> map){

        try{
            List<PayOrderVO> list= payService.getPayOrder(map.get("userId"));
            return ResultEntity.successWithData(list);
        }catch (Exception e){
            return ResultEntity.falseWithoutData(e.getMessage());
        }

    }

    @PostMapping("/pay/delete/payorder")
    @ApiOperation(value = "用户删除支付成功的历史订单", notes ="需要接收用户的id，以及要删除的订单id")
    @ResponseBody
    public ResultEntity<String> doDeleteUserOrder(@RequestBody Map<String,String> map){

        try{
            PayOrderPO payOrderPO=new PayOrderPO();
            payOrderPO.setUserId(map.get("userId"));
            payOrderPO.setTrade_no(map.get("tradeNo"));
            payService.deletePayOrder(payOrderPO);
            return ResultEntity.successWithoutData();
        }catch (Exception e){
            return ResultEntity.falseWithoutData(e.getMessage());
        }


    }




}

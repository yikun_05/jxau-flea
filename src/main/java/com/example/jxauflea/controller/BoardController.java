package com.example.jxauflea.controller;

import com.example.jxauflea.annotation.SystemControllerLog;
import com.example.jxauflea.pojo.BoardPO;
import com.example.jxauflea.pojo.GoodsPO;
import com.example.jxauflea.service.BoardService;
import com.example.jxauflea.util.ResultEntity;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
public class BoardController {

    @Autowired
    private BoardService boardService;

/*
    // 分页版本
    @GetMapping("/board/get/all/message")
    @ApiOperation(value = "查看留言板上的所有信息")
    public ResultEntity<PageInfo<BoardPO>> doGetMessage(@RequestParam(value = "pageNum",required = false,defaultValue ="1")Integer pageNum,
                                                        @RequestParam(value = "pageSize",required = false,defaultValue ="5")Integer pageSize){
        // 需要进行分页操作
        try{
            PageHelper.startPage(pageNum,pageSize);// 下一句一定得是service调用方法，不然报错
            List<BoardPO> Boards=boardService.getAll();
            PageInfo<BoardPO> pageInfo = new PageInfo<>(Boards);
            return ResultEntity.successWithData(pageInfo);

        }catch (Exception e){
            return ResultEntity.falseWithoutData(e.getMessage());
        }


    }*/

    @SystemControllerLog(actionType = "用户查看",descrption = "用户查看留言板信息")
    @GetMapping("/board/get/all/message")
    @ApiOperation(value = "查看留言板上的所有信息")
    public ResultEntity<List<BoardPO>> doGetMessage(){
        // 需要进行分页操作
        try{
            List<BoardPO> Boards=boardService.getAll();
            return ResultEntity.successWithData(Boards);

        }catch (Exception e){
            return ResultEntity.falseWithoutData(e.getMessage());
        }


    }


    @SystemControllerLog(actionType = "用户发表",descrption = "用户发表留言内容")
    @PostMapping("/board/user/add/message")
    @ApiOperation(value = "用户发表自己的留言", notes ="需要接收用户的id,发表留言的内容")
    public ResultEntity<String> doSaveMessage(@RequestBody Map<String,String> map){

        try{
            String userId = map.get("userId");
            String comment = map.get("comment");
            BoardPO boardPO=new BoardPO();
            boardPO.setComment(comment);
            boardPO.setUserId(userId);
            boardPO.setUuid(UUID.randomUUID().toString());
            boardService.saveMessage(boardPO);
            return ResultEntity.successWithoutData();

        }catch (Exception e){
            return ResultEntity.falseWithoutData(e.getMessage());
        }

    }


}

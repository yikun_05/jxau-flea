package com.example.jxauflea.controller;

import com.example.jxauflea.pojo.CommentPO;
import com.example.jxauflea.service.CommentService;
import com.example.jxauflea.util.ResultEntity;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@RestController
public class CommentController {

    @Autowired
    private CommentService commentService;

    @ApiOperation
            (
                    value ="评论",
                    notes ="需要返回一个评论对象comment(有uuid, goodsId, content, parent_uuid, userId, createTime，6个字段)"
            )
    @PostMapping("/comment/commit")
    public ResultEntity doCommentCommit(@RequestBody HashMap<String,Object> researchMap) {
        CommentPO comment = new CommentPO();
        comment.setUuid(String.valueOf(UUID.randomUUID()));//添加uuid
        comment.setGoodsId(researchMap.get("goodsId").toString());
        if (researchMap.get("called_userName") != null) {
            comment.setCalled_userName(researchMap.get("called_userName").toString());
        } else {
            comment.setCalled_userName(null);
        }
        comment.setContent(researchMap.get("content").toString());
        comment.setParent_uuid(researchMap.get("parent_uuid").toString());
        comment.setUserId(researchMap.get("userId").toString());
        comment.setSeller(commentService.judge(comment));//判断是否为卖家
        commentService.addComment(comment);//将对象添加进入数据库
        return ResultEntity.successWithoutData();
    }

    @ApiOperation
            (
                    value ="评论展示",
                    notes ="无需参数"
            )
    @GetMapping("/comment/show")
    public ResultEntity<List<CommentPO>> showComment(@RequestParam("goodsId")String goodsId) {
        return ResultEntity.successWithData(commentService.listTop(goodsId));

    }



}

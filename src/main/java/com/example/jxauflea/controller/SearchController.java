package com.example.jxauflea.controller;

import com.example.jxauflea.annotation.SystemControllerLog;
import com.example.jxauflea.pojo.GoodsVO;
import com.example.jxauflea.service.SearchService;
import com.example.jxauflea.util.ResultEntity;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
public class SearchController {

    @Autowired
    private SearchService searchService;


    @ApiOperation
            (
            value = "通过商品的名称来进行模糊查询",
            notes ="需要提供商品的大概名称,以及查询状态state(0：不排序,1：按照价格排序，2：按照时间排序),查询区间min,max,可以不传"
            )
    @PostMapping("/search/byName")
    public ResultEntity<List<GoodsVO>> doSearchByName(@RequestBody HashMap<String,String> researchMap) {
        //如果有传入最大值最小值，则赋值，没有则都为0,可以相等
        int min = (researchMap.get("min")== null ? 0:Integer.parseInt(researchMap.get("min")));
        int max = (researchMap.get("max")== null ? 0:Integer.parseInt(researchMap.get("max")));
        int state = (researchMap.get("state") == null ? 0 : Integer.parseInt(researchMap.get("state")));
        List<GoodsVO> goodsVOS = searchService.selectByKey(researchMap.get("goodsName"),state,min,max);
        List<GoodsVO> result = new ArrayList<>();

        for (GoodsVO goodsVO : goodsVOS) {
            goodsVO.setImgUrls(searchService.selectImgByGoodsId(goodsVO.getUuid()));
            result.add(goodsVO);
        }

        return ResultEntity.successWithData(result);
    }


    @ApiOperation
            (
                    value = "通过商品的类型来查询",
                    notes ="需要提供商品的类型,以及查询状态state(0：不排序,1：按照价格排序，2：按照时间排序),查询区间min,max,可以不传"
            )
    @PostMapping("/search/byType")
    public ResultEntity<List<GoodsVO>> doSearchByType(@RequestBody HashMap<String,String> researchMap) {
        //如果有传入最大值、最小值、状态，则赋值，没有则都为0,max和min可以相等
        int min = (researchMap.get("min")== null ? 0:Integer.parseInt(researchMap.get("min")));
        int max = (researchMap.get("max")== null ? 0:Integer.parseInt(researchMap.get("max")));
        int state = (researchMap.get("state") == null ? 0 : Integer.parseInt(researchMap.get("state")));
        List<GoodsVO> goodsVOS = searchService.selectByType(researchMap.get("type"),state,min,max);
        List<GoodsVO> result = new ArrayList<>();

        for (GoodsVO goodsVO : goodsVOS) {
            goodsVO.setImgUrls(searchService.selectImgByGoodsId(goodsVO.getUuid()));
            result.add(goodsVO);
        }

        return ResultEntity.successWithData(result);
    }


    @ApiOperation
            (
                    value = "猜你喜欢",
                    notes ="提供查询状态state(0：不排序,1：按照价格排序，2：按照时间排序)"
            )
    @PostMapping("/search/byLove")
    public ResultEntity<List<GoodsVO>> doSearchByLove(@RequestBody HashMap<String,String> researchMap) {
        //如果有传入最大值、最小值、状态，则赋值，没有则都为0,max和min可以相等
        int state = (researchMap.get("state") == null ? 0 : Integer.parseInt(researchMap.get("state")));
        List<GoodsVO> goodsVOS = searchService.selectByLove(state);
        List<GoodsVO> result = new ArrayList<>();

        for (GoodsVO goodsVO : goodsVOS) {
            goodsVO.setImgUrls(searchService.selectImgByGoodsId(goodsVO.getUuid()));
            result.add(goodsVO);
        }

        return ResultEntity.successWithData(result);
    }

}

package com.example.jxauflea.controller;

import com.example.jxauflea.pojo.GoodsVO;
import com.example.jxauflea.pojo.OrderPO;
import com.example.jxauflea.service.OrderService;
import com.example.jxauflea.util.ResultEntity;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/order/get/user/sell")
    @ApiOperation(value = "查看出售商品的历史订单",notes ="需要参数用户的id")
    public ResultEntity<List<GoodsVO>> doGetUserSell(@RequestBody Map<String,String> map){

        try{

            List<GoodsVO> list= orderService.getUserSell(map.get("userId"));
            return ResultEntity.successWithData(list);
        }catch (Exception e){
            return ResultEntity.falseWithoutData(e.getMessage());
        }

    }

    @PostMapping("/order/delete/user/sell")
    @ApiOperation(value = "删除出售商品的历史订单",notes ="需要参数用户的id")
    public ResultEntity<List<OrderPO>> doDeleteUserSell(@RequestBody Map<String,String> map){

        try{
            orderService.deleteUserSell(map.get("userId"));
            return ResultEntity.successWithoutData();
        }catch (Exception e){
            return ResultEntity.falseWithoutData(e.getMessage());
        }

    }


}


package com.example.jxauflea.controller;

import com.example.jxauflea.annotation.SystemControllerLog;
import com.example.jxauflea.pojo.Address;
import com.example.jxauflea.pojo.UserPO;
import com.example.jxauflea.service.GoodsService;
import com.example.jxauflea.service.UserService;
import com.example.jxauflea.util.Context;
import com.example.jxauflea.util.ResultEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author chenzouquan
 */
@RestController
public class UserController {

    @Resource
    private UserService userService;

    @SystemControllerLog(actionType = "用户操作",descrption = "用户登录")
    @PostMapping("/user/login")
    public ResultEntity<Object> userLogin(@RequestBody HashMap<String,String> userMap, HttpSession httpSession){
        ResultEntity<Object> userLogin = userService.userLogin(userMap.get("username"), userMap.get("password"));
        if(ResultEntity.SUCCESS.equals(userLogin.getResult())){
            httpSession.setAttribute("user",userLogin.getData());
        }
        return userLogin;
    }

    @SystemControllerLog(actionType = "用户操作",descrption = "用户注册")
    @PostMapping("/user/addOneUser")
    public ResultEntity<Object> userAdd(@RequestBody HashMap<String,String> userMap,HttpSession httpSession){
        ResultEntity<Object> objectResultEntity = userService.userAdd(userMap.get("userName"), userMap.get("password"));
        if (ResultEntity.SUCCESS.equals(objectResultEntity.getResult())){
            httpSession.setAttribute("user",objectResultEntity.getData());
        }
        return objectResultEntity;
    }
    @Async
    @SystemControllerLog(actionType = "用户操作",descrption = "用户发短信")
    @PostMapping("/user/send/short/message")
    public ResultEntity<String> sendMessage(@RequestBody HashMap<String,String> map){
        return userService.userSendMessage(map.get("phone"));
    }

    @SystemControllerLog(actionType = "用户操作",descrption = "用户忘记密码")
    @PostMapping("/user/forget/password")
    public ResultEntity<String> forgetPassword(@RequestBody HashMap<String,String> userMap){
        ResultEntity<String> stringResultEntity = userService.verifySMS(userMap.get("phone"), userMap.get("code"));
        String result = stringResultEntity.getResult();
        if(ResultEntity.FALSE.equals(result)){
            return ResultEntity.falseWithoutData("验证码错误");
        }else{
            return userService.updatePassword(userMap.get("phone"),userMap.get("phone").substring(7,11));
        }
    }

    @SystemControllerLog(actionType = "用户操作",descrption = "用户修改密码")
    @PostMapping("/user/update/password")
    public ResultEntity<String> updatePassword(@RequestBody HashMap<String,String> userMap){
         return userService.updateUser(userMap,null,false);
    }

    @SystemControllerLog(actionType = "用户操作",descrption = "用户打开浏览历史")
    @PostMapping("/user/update/historyOpen")
    public ResultEntity<String> updateHistoryOpen(@RequestBody HashMap<String,String> userMap){
         return userService.updateUser(userMap,null,false);
    }

    @SystemControllerLog(actionType = "用户操作",descrption = "用户添加收货地址")
    @PostMapping("/user/add/address")
    public ResultEntity<String> addAddress(@RequestBody HashMap<String,String> userMap){
        return userService.updateUser(userMap,null,false);
    }

    @SystemControllerLog(actionType = "用户操作",descrption = "用户完善个人信息")
    @PostMapping("/user/update/userInfo")
    public ResultEntity<String> updateUserInfo(@RequestParam("userName") String username, @RequestParam("phone") String phone,@RequestParam("userId") String userId, @RequestParam(required = false,value = "imgUrl") MultipartFile file){
        HashMap userMap = new HashMap<String,String>(3);
        userMap.put("username",username);
        userMap.put("userId",userId);
        userMap.put("phone",phone);
        return userService.updateUser(userMap,file,true);
    }

    @SystemControllerLog(actionType = "用户操作",descrption = "查找用户最新信息")
    @PostMapping("/user/found/one/user")
    public ResultEntity<UserPO> getOneUser(@RequestBody HashMap<String,String>hashMap){
        ResultEntity<UserPO> userId = userService.getOneUser(hashMap.get("userId"));
        UserPO data = userId.getData();
        Address mu = data.getAddressInfo();
        mu.setAddressDetail(data.getAddress());
        mu.setName(data.getRname());
        mu.setTel(data.getPhone());
        return ResultEntity.successWithData(data);
    }
}

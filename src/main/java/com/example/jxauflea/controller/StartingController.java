package com.example.jxauflea.controller;

import com.example.jxauflea.pojo.StartVo;
import com.example.jxauflea.service.StartingService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StartingController {

    @Autowired
    private StartingService startingService;

    @PostMapping("/")
    @ApiOperation(value = "初始化访问界面,会返回十张图片和其对应用户的名称头像等信息", notes ="无需参数")
    public List<StartVo> Starting() {

        return startingService.Starting();

    }

}

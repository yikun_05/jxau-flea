package com.example.jxauflea.controller;

import com.example.jxauflea.pojo.GoodsListVO;
import com.example.jxauflea.pojo.UserPO;
import com.example.jxauflea.pojo.UserVo;
import com.example.jxauflea.service.ManagerService;
import com.example.jxauflea.service.UserService;
import com.example.jxauflea.util.ResultEntity;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class MangeController {

    @Resource
    private UserService userService;

    @Autowired
    private ManagerService managerService;

    @ApiOperation(value = "管理员登录")
    @PostMapping("/manger/index")
    public String checkManger(HttpSession session, @RequestParam("userName") String userName, @RequestParam("password") String password,Model model) {

        ResultEntity<Object> userLogin = userService.userLogin(userName, password);
        if(userLogin.getData()!=null){

            session.setAttribute("manger", userLogin.getData());
            return "index1";
        }else{
            model.addAttribute("message","用户不存在或密码错误！");
            return "login";
        }
    }

    // 渲染用户数据
    @ApiOperation(value = "获取用户列表并前往对应页面")
    @RequestMapping("member-list")
    public String getmemberlist(Model model,HttpSession session){

        UserPO manger =(UserPO) session.getAttribute("manger");
        int state = manger.getState();
        if(state==4){

            List<UserVo> userVos = managerService.listAllByRoot();
            model.addAttribute("userlist",userVos);

        }else if(state==3){

            List<UserVo> userVos = managerService.listAllByAdmin();
            model.addAttribute("userlist",userVos);

        }

        return "member-list";
    }

    // 获取商品管理列表
    @ApiOperation(value = "获取商品列表并前往对应页面")
    @GetMapping("/get/goods-list")
    public String getgoodslist(Model model){

        List<GoodsListVO> goodsListVOS = managerService.managerGetGoodsList();
        model.addAttribute("goodsList",goodsListVOS);
        return "goods-list";

    }

}

package com.example.jxauflea.controller;

import com.example.jxauflea.pojo.LogPO;
import com.example.jxauflea.service.LogService;
import com.example.jxauflea.util.ResultEntity;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class LogController {

    @Autowired
    private LogService logService;

    @ApiOperation(value = "获取日志列表并前往对应页面")
    @RequestMapping("/log/mange/list")
    public String doLogList(Model model){

        List<LogPO> list= logService.getAllLogs();
        if(list!=null) model.addAttribute("logList",list);
        return "Logs-list";
    }

    @ApiOperation(value = "根据日志编号删除对应的日志")
    @RequestMapping("/log/delete")
    @ResponseBody
    public ResultEntity<String> doLogList(String logId){

        try{
            logService.deleteLogById(logId);
            return ResultEntity.successWithoutData();
        }catch (Exception e){
            return ResultEntity.falseWithoutData(e.getMessage());
        }
    }


    @ApiOperation(value = "根据日志内容查找相应的数据")
    @RequestMapping("/log/search/by/content")

    public String doSearchLogList(String content,Model model){

        List<LogPO> list=logService.searchByContent(content);
        if(list!=null) model.addAttribute("logList",list);
        return "Logs-list.html";
    }

}

package com.example.jxauflea.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RouterController {

    @RequestMapping("/")
    public String toTest(){
        return "index";
    }


    @RequestMapping("/login")
    public String getLogin(){
        return "login";
    }

    @RequestMapping("welcome/index")
    public String getWelcome(){
        return "welcome";
    }

    @RequestMapping("get/index")
    public String getindex(){
        return "index1";
    }



}

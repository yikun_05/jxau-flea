package com.example.jxauflea.util;

import java.util.concurrent.*;

public class Context {
    public static final String INFO_IMAGE ="http://jxau7124.oss-cn-shenzhen.aliyuncs.com/20211210/dc0ff16d7227465fa4d4ba5009304e57.png";
    public static final String APP_KEY = "204000764";
    public static final String APP_SECRET = "HGgVHoTtHsUxJyaBuzsPQ3eXK4lf6kSh";
    public static final String APP_CODE = "8b22f317d3424fa48e867075ad0b472f";
    /**核心线程池大小*/
    public static final int CORE_POOL_SIZE = 20;
    /**最大线程池大小*/
    public static final int MAXIMUM_POOL_SIZE = 50;
    /**超时时间 超时了没有人调用就会释放*/
    public static final long KEEP_ALIE_TIME = 4L;
    /**超时单位*/
    public static final TimeUnit UNIT = TimeUnit.MINUTES;
    /**阻塞队列*/
    public static  BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<Runnable>(10);
    /**拒绝策略*/
    public static  RejectedExecutionHandler handler = new ThreadPoolExecutor.AbortPolicy();

}
